
package calculadora;

import java.util.Scanner;

public class Calculadora {
    
    public static void main(String[] args) {
        Metodos m = new Metodos();
        Scanner in = new Scanner(System.in);
        int n1=6,n2=2,result,opc;
        boolean op = true;
        while (op) {
            System.out.println("");
            System.out.println("-----Calculadora-----");
            System.out.println("1. Sumar");
            System.out.println("2. Restar");
            System.out.println("3. Multiplicar");
            System.out.println("4. Dividir");
            System.out.println("5. Salir");
            System.out.println("Opcion: ");
            opc = in.nextInt();
            
            if(opc==5){
                System.out.println("-----Finalizado-----");
                break;
            }
            else{
                switch (opc) {
                    case 1:                         
                        result=m.Sumar(n1, n2);
                        System.out.println("El resultado es: " +result);
                        break;
                        
                    case 2:
                        result=m.Restar(n1, n2);
                        System.out.println("El resultado es: " +result);
                        break;
                        
                    case 3:
                        result=m.Multiplicar(n1, n2);
                        System.out.println("El resultado es: " +result);
                        break;
                        
                    case 4:
                        result=m.Dividir(n1, n2);
                        System.out.println("El resultado es: " +result);
                        break;
                        
                    default:
                        System.out.println("*** Opcion No Valida ***");
                        System.out.println("*** Intente Nuevamente ***");
                }
            }
        }
    }
}
